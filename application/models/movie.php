<?php

class Movie extends Koz 
{ 	
	public static $timestamps = true;
	
	public function category()
	{
		return $this->belongs_to('Category');
	}
	
	public function genres()
	{
		return $this->has_many_and_belongs_to('Genre', 'movies_genres');
	}  	
		
	public function countries()
	{
		return $this->has_many_and_belongs_to('Country', 'movies_countries');
	}
	
	public function tags()
	{
		return $this->has_many_and_belongs_to('Tag', 'movies_tags');
	}
	
	public function translations()
	{
		return $this->has_many_and_belongs_to('Translation', 'movies_translations');
	}
	
	public function related_movies()
	{
		return $this->has_many_and_belongs_to('Movie', 'related_movies');
	}
	
	public function files()
	{
		return $this->has_many('File');
	}
	
	public function seasons()
	{
		return $this->has_many('Season');
	}	
	
		
}