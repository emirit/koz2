<?php
class Country extends Koz
{
	public function movies()
	{
		return $this->has_many_and_belongs_to('Movie', 'movies_countries');
	}	
}