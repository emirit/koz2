<?php
  return array(

	/*
	|--------------------------------------------------------------------------
	| Pagination Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the pagination links. You're free to change them to anything you want.
	| If you come up with something more exciting, let us know.
	|
	*/

	'welcome' => 'Добро пожаловать',
	'site_name' => 'KOZ.TV - восточные фильмы онлайн',
	'choose_category' => 'Выберите категорию',
	'category' => 'Категория',
	'choose_genre'     => 'Выберите жанр',
	'genre'     => 'Жанр',
	'choose_country'     => 'Выберите страну',
	'country'     => 'Страна',
	'choose_translation'     => 'Выберите язык',
	'translation'     => 'Язык',
	'choose_year'     => 'Выберите год',
	'year'     => 'Год',
	'enter_movie_name'     => 'Введите название фильма',
	'search_submit'     => 'Искать',
	'new_items'     => 'Новинки',
	'related_items'     => 'Похожие',
	'top'     => 'Топ 5',
	'search_reset'     => 'Сброс',
	'age_limit'     => 'Ограничения по возрасту',

);
?>