<?php
class Filter_Controller extends Base_Controller 
{
	public $restful = true;
 
	public function get_build_filters($params)
	{
		
		if(trim($params) != '')
		{	
			if(!Cache::get($params))
			{
				$tmp = explode('_', $params);
				$model = $tmp[0];
				$slug = $tmp[1];
				
				$object = $model::where('slug', '=', $slug)->first();
				
				$movies = $object->movies()->get();
			
				$filters = array();	
									
				if(is_array($movies))
				{
					foreach($movies as $movie)
					{
						$category = $movie->category()->first();
						
						$filters['category_slug'][$category->slug] = $category->name;						
						
						foreach($movie->genres()->get() as $genre)
						{
							$filters['genre_slug'][$genre->slug] = $genre->name;						
						}
						
						foreach($movie->translations()->get() as $translation)
						{
							$filters['translation_slug'][$translation->slug] = $translation->name;						
						}
						
						foreach($movie->countries()->get() as $country)
						{
							$filters['country_slug'][$country->slug] = $country->name;						
						}
						
						foreach($movie->tags()->get() as $tag)
						{
							$filters['tag_slug'][$tag->slug] = $tag->name;						
						}	
						
					}
				}
				
				Cache::forever($params, $filters);				
			}
			
			$filters = Cache::get($params);
			
			$callback = array_key_exists('callback', $_GET) ? $_GET['callback'] : '';
			
			if($callback)
			{
				return Response::jsonp($callback, $filters);
			}
			else
			{
				return Response::json($filters);
			}
						
			
		}
	}
	
	public function get_all($model)
	{
		$result = array();
		foreach($model::all() as $item)
		{
			$result[] = array('slug' => $item->slug, 'name' => $item->name);	
		}
		return Response::json($result);		
	}
	
	
	 
  
}