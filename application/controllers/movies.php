<?php
class Movies_Controller extends Base_Controller 
{
	public $restful = true;
	private $meta_title = '';
	
	private function movies_by_params($params)
	{
		$categories = Utils::make_slug_keys(Category::order_by('id')->get());		
		$genres = Utils::make_slug_keys(Genre::order_by('name')->get());
		$countries = Utils::make_slug_keys(Country::order_by('name')->get());
		$translations = Utils::make_slug_keys(Translation::order_by('name')->get());	
		
		
		$movies_obj = DB::table('movies');
		$breadcrumbs = array();
		$cache_key = 'movies';
		
		if(isset($params['category']))
		{
			$movies_obj = $movies_obj->join('categories','categories.id', '=', 'movies.category_id');
			$movies_obj = $movies_obj->where('categories.slug','=', $params['category']);
			$this->meta_title .= __('common.category') . ': ' . $categories[$params['category']]->name . '. ';
			$cache_key .= '_' . $params['category'];
		}
		
		if(isset($params['genre']))
		{
			$movies_obj = $movies_obj->join('movies_genres','movies_genres.movie_id', '=', 'movies.id');
			$movies_obj = $movies_obj->join('genres','movies_genres.genre_id', '=', 'genres.id');
			$movies_obj = $movies_obj->where('genres.slug','=', $params['genre']);
			$this->meta_title .= __('common.genre') . ': ' . $genres[$params['genre']]->name . '. ';
			$cache_key .= '_' . $params['genre'];
		}
		
		if(isset($params['country']))
		{
			$movies_obj = $movies_obj->join('movies_countries','movies_countries.movie_id', '=', 'movies.id');
			$movies_obj = $movies_obj->join('countries','movies_countries.country_id', '=', 'countries.id');
			$movies_obj = $movies_obj->where('countries.slug','=', $params['country']);
			$this->meta_title .= __('common.country') . ': ' . $countries[$params['country']]->name . '. ';
			$cache_key .= '_' . $params['country'];
		}
		
		if(isset($params['translation']))
		{
			$movies_obj = $movies_obj->join('movies_translations','movies_translations.movie_id', '=', 'movies.id');
			$movies_obj = $movies_obj->join('translations','movies_translations.translation_id', '=', 'translations.id');
			$movies_obj = $movies_obj->where('translations.slug','=', $params['translation']);
			$this->meta_title .= __('common.translation') . ': ' . $translations[$params['translation']]->name . '. ';
			$cache_key .= '_' . $params['translation'];
		}
		
		if(isset($params['tag']))
		{
			$movies_obj = $movies_obj->join('movies_tags','movies_tags.movie_id', '=', 'movies.id');
			$movies_obj = $movies_obj->join('tags','movies_tags.tag_id', '=', 'tags.id');
			$movies_obj = $movies_obj->where('tags.slug','=', $params['tag']);
			$this->meta_title .= __('common.tag') . ': ' . $tags[$params['tag']]->name . '. ';
			$cache_key .= '_' . $params['tag'];
		}
		
		if(isset($params['year']))
		{
			$years = explode('-', $params['year']);
			$movies_obj = $movies_obj->where_between('movies.year', $years[0], $years[1]);
			$cache_key .= '_' . $params['year'];	
		}
		
		if(isset($params['name']))
		{   						
			$movies_obj = $movies_obj->where('movies.name', 'LIKE', '%' . urldecode($params['name']) . '%');
			$cache_key .= '_' . $params['name'];	
		}
		
		if(!Cache::get($cache_key))
		{   			
			Cache::forever($cache_key, $movies_obj->get(array('movies.id')));				
		}	
		
		$movies_ids = Cache::get($cache_key);
		
		
		$movies = array();		
		foreach($movies_ids as $movie)
		{
			if(!Cache::get('movie_' . $movie->id))
			{
				$movie_item = Movie::find($movie->id);		
					
				$movie_item->category = $movie_item->category()->get();
				$movie_item->genres = $movie_item->genres()->get();			
				$movie_item->tags = $movie_item->tags()->get();
				$movie_item->countries = $movie_item->countries()->get();
				$movie_item->translations = $movie_item->translations()->get();
				
				Cache::forever('movie_' . $movie->id, $movie_item);	
			}   
			
			$movies[] = Cache::get('movie_' . $movie->id);
		}
		
		return $movies; 		
	}
 
	public function get_index($path = 'all')
	{	
		$parts = explode('/', $path);
		$params = array();	
		
		if($path != 'all')
		{
			foreach($parts as $part)
			{
				$tmp = explode('_', $part);
				$model = $tmp[0];
				$slug = $tmp[1];
				$params[$model] = $slug;
			}
		}
		$movies = $this->movies_by_params($params);
		
		$new_movies = Movie::order_by('created_at', 'desc')->take(5)->get();              		
		$top = Movie::order_by(DB::raw('(likes - dislikes)'), 'desc')->take(5)->get();    			
		
						
		return View::make('frontend.movies')			           
			           ->with('new_movies', $new_movies)
			           ->with('movies', $movies)
			           ->with('top', $top)  			           
			           ->with('params', $params)
			           ->with('meta_title', $this->meta_title)
		           ;    		
	}
	
	
	
	public function get_movies_json($path = 'all')
	{   
		$parts = explode('/', $path);
		$params = array();	
		
		if($path != 'all')
		{
			foreach($parts as $part)
			{
				$tmp = explode('_', $part);
				$model = $tmp[0];
				$slug = $tmp[1];
				$params[$model] = $slug;
			}
		}
		$movies = $this->movies_by_params($params);
		
		$callback = array_key_exists('callback', $_GET) ? $_GET['callback'] : '';
			
		if($callback)
		{
			return Response::jsonp($callback, $movies);
		}
		else
		{
			return Response::json($movies);
		}	
	}
	
	
	public function get_single($slug)
	{
		if(!$slug)
		{
			return Response::error('404');
		}
		
		$categories = Category::all();		
		$movie = Movie::where('slug', '=', $slug)->first();
        if(!$movie)
		{
			return Response::error('404');
		}
		
		$meta_title = $movie->name;
		
		return View::make('frontend.movie')  						
						->with('movie', $movie)
						->with('meta_title', $meta_title);
	}
	
	public function get_single_json($slug)
	{
		$movie = Movie::where('slug', '=', $slug)->first();
		
        if(!$movie)
		{
			return Response::error('404');
		}
		
		$movie->genres = $movie->genres()->get();
		$movie->countries = $movie->countries()->get();
		$movie->translations = $movie->translations()->get();
		
		$callback = array_key_exists('callback', $_GET) ? $_GET['callback'] : '';
			
		if($callback)
		{
			return Response::jsonp($callback, $movie);
		}
		else
		{
			return Response::json($movie);
		}	
	}
	
	public function get_video($slug)
	{   		
		if(!$slug)
		{
			return Response::error('404');
		}
		$movie = Movie::where('slug', '=', $slug)->first();
		         
		if(!$movie->video)
		{
			return Response::error('404');
		}
		 		
		$path = path('storage') . 'videos/' . $movie->video; 
		header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename='.basename($path));
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($path));
	    ob_clean();
	    flush();
	    readfile($path);
	    exit;

		  
		
		//return Response::download($path);    
	}
	 
  	
  	 
  	 
  
}