<?php
class User_Controller extends Base_Controller {
	
	public $restful = true;
 
	public function get_index()
	{
		if(Auth::check())
		{
			return View::make('frontend.user_profile')->with('meta_title', __('common.profile'));		
		}
		else
		{
			return View::make('frontend.user_login')->with('meta_title', __('common.login'));					
		}
		
	}
	
	public function post_login()
	{
		$rules = array('email' => 'required|email', 'password' => 'required'); 
		$validation = Validator::make(Input::all(), $rules); 
		if ($validation->fails()) 
		{ 
			return Redirect::to('user/login/')->with_errors($validation); 
		}
		else
		{
			$credentials = array('username' => Input::get('email'), 'password' => Input::get('password'));
     
		    if (Auth::attempt($credentials))
		    {
		    	return Redirect::to('user');
		    }
		    else
		    {
				return Redirect::to('user/login/');
		    }
		}
	}
	
	public function get_logout()
	{
		Auth::logout();
		
		return Redirect::to('user/login/');
	}
	
}