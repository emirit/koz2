<?php

class Home_Controller extends Base_Controller {

	/*
	|--------------------------------------------------------------------------
	| The Default Controller
	|--------------------------------------------------------------------------
	|
	| Instead of using RESTful routes and anonymous functions, you might wish
	| to use controllers to organize your application API. You'll love them.
	|
	| This controller responds to URIs beginning with "home", and it also
	| serves as the default controller for the application, meaning it
	| handles requests to the root of the application.
	|
	| You can respond to GET requests to "/home/profile" like so:
	|
	|		public function action_profile()
	|		{
	|			return "This is your profile!";
	|		}
	|
	| Any extra segments are passed to the method as parameters:
	|
	|		public function action_profile($id)
	|		{
	|			return "This is the profile for user {$id}.";
	|		}
	|
	*/ 
	
		
	public function action_index()
	{
		
		//$new_movies = Movie::order_by('created_at', 'desc')->take(18)->get();		
		//$top = Movie::order_by(DB::raw('(likes - dislikes)'), 'desc')->take(5)->get();		
		//$genres_on_main = Genre::where_on_main(true)->get();                          		
		$meta_title = __('common.welcome');	
		
		return View::make('frontend.home')			           
			          // ->with('new_movies', $new_movies)
			          // ->with('genres_on_main', $genres_on_main)
			          // ->with('top', $top)
			           ->with('meta_title', $meta_title);
	}

}