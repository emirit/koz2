<?php

class Create_Basic_Table {

	
	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
	    
	    Schema::table('categories', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->string('name', 255);
		    $table->string('slug', 255);
		    $table->text('description');		
		    
		    $table->timestamps();    
	    });
	    
	    Schema::table('genres', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->string('name', 255);
		    $table->string('slug', 255);
		    $table->text('description');	
		    
		    $table->timestamps();	    
	    });
	    
	    Schema::table('tags', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->string('name', 255)->unique();
		    $table->string('slug', 255);
		    $table->text('description');	
		    
		    $table->timestamps();	    
	    });
	    
	    Schema::table('countries', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->string('name', 255);
		    $table->string('slug', 255);
		    $table->text('description');
		    
		    $table->timestamps();		    
	    });
	    
	    Schema::table('translations', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->string('name', 255);
		    $table->string('slug', 5);
		    
		    $table->timestamps();
		    		    
	    }); 
	    	    	            
		Schema::table('movies', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id');
		    $table->string('name', 255);
		    $table->string('name_original', 255);
		    $table->string('thumbnail', 255);
		    $table->string('video', 255);
		    $table->text('video_embed');
		    $table->integer('age');
		    $table->string('slug', 255);
		    $table->text('description');
		    $table->integer('year');		    
		    $table->integer('views');		    
		    $table->integer('likes');		    
		    $table->integer('dislikes');		    
		    $table->boolean('featured');		    
		    $table->integer('category_id')->unsigned();		    
		    $table->timestamps();
		    
		    $table->foreign('category_id')->references('id')->on('categories');
	    });
	    
	    Schema::table('files', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->string('name', 255);
		    $table->string('type', 5);
		    $table->text('description');
		    $table->integer('movie_id')->unsigned();
		    $table->timestamps();
		    		    
		    $table->foreign('movie_id')->references('id')->on('movies');		    		    
	    });
	    
	    
	    // linking tables
	    
	    Schema::table('movies_genres', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->integer('movie_id')->unsigned();		    
		    $table->integer('genre_id')->unsigned();
		    
		    $table->timestamps();
		    		    
		    $table->foreign('movie_id')->references('id')->on('movies');		    		    
		    $table->foreign('genre_id')->references('id')->on('genres');		    		    
	    });
	    
	    Schema::table('movies_translations', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->integer('movie_id')->unsigned();		    
		    $table->integer('translation_id')->unsigned();
		    
		    $table->timestamps();
		    		    
		    $table->foreign('movie_id')->references('id')->on('movies');		    		    
		    $table->foreign('translation_id')->references('id')->on('translations');		    		    
	    });
	    
	
		
		Schema::table('movies_countries', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->integer('movie_id')->unsigned();		    
		    $table->integer('country_id')->unsigned();
		    
		    $table->timestamps();
		    		    
		    $table->foreign('movie_id')->references('id')->on('movies');		    		    
		    $table->foreign('country_id')->references('id')->on('countries');		    		    
	    });
	    
	    Schema::table('movies_tags', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->integer('movie_id')->unsigned();		    
		    $table->integer('tag_id')->unsigned();

			$table->timestamps();		    		    
		    		    
		    $table->foreign('movie_id')->references('id')->on('movies');		    		    
		    $table->foreign('tag_id')->references('id')->on('tags');		    		    
	    });
	    
	    
	    
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		// 		
		
		Schema::drop('movies_tags');		
		Schema::drop('movies_countries');
		Schema::drop('movies_genres');
		Schema::drop('tags');
		Schema::drop('translations');		
		Schema::drop('countries');
		Schema::drop('genres');
		Schema::drop('files');
		Schema::drop('movies');		
		Schema::drop('categories');
		
	}

}