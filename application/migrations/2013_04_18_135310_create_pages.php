<?php

class Create_Pages {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('pages', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->string('name', 255);
		    $table->string('slug', 255);
		    $table->string('meta_title', 255);
		    $table->string('meta_keywords', 255);
		    $table->string('meta_description', 255);
		    $table->text('body');		    
	    });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('pages');
	}

}