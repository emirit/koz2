<?php

class Users_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		//
		Schema::table('users', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->string('name', 255);
		    $table->string('email', 255);
		    $table->string('password', 255);
		    $table->boolean('active'); 	
		    $table->integer('role'); 	
		    
		    $table->timestamps();  
		    
		    		    
	    });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('users');
	}

}