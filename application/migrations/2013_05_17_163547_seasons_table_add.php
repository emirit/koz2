<?php

class Seasons_Table_Add {

	public function up()
	{
		//
		Schema::table('seasons', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->string('name', 255);		    
		    $table->integer('movie_id')->unsigned();		    
		    $table->timestamps();
		    
		    $table->foreign('movie_id')->references('id')->on('movies')->on_delete('cascade');
		    	    
	    });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('seasons');
	}

}