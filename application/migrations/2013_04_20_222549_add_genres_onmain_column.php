<?php

class Add_Genres_Onmain_Column {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('genres', function($table)
	    {   	    	
		    $table->boolean('on_main'); 		    		    
	    });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}