<?php

class Series_Table_Add {

	public function up()
	{
		//
		Schema::table('series', function($table)
	    {
	    	$table->engine = 'InnoDB';
		    $table->create();
		    $table->increments('id')->unsigned();
		    $table->string('name', 255);
		    $table->string('video', 255);
		    $table->text('video_embed');
		    $table->integer('season_id')->unsigned();		    
		    $table->timestamps();
		    
		    $table->foreign('season_id')->references('id')->on('seasons')->on_delete('cascade');
		    	    
	    });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('series');
	}

}