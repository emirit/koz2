<li class="landscape">
	<a href="/movie/{{ $movie->slug }}/" title="{{ $movie->name }}">
		<img src="/public/uploads/images/thumbs/landscape/{{ $movie->thumbnail }}" alt="{{ $movie->name }}" />
				<div class="movie-info">
			<div class="movie-name"><b>{{ $movie->id }}</b> {{ $movie->name }}</div>
			<div class="movie-params">
			
				@foreach ($movie->genres()->get() as $genre)
    				<span class="label genre">{{ $genre->name_single }}</span>
    			@endforeach 
    		    		
    		    @foreach ($movie->countries()->get() as $country)
    				<span class="label label-info country">{{ $country->name }}</span>
    			@endforeach
    				<span class="label label-warning year">{{ $movie->year }}</span>
    		</div>    		
		</div>
	</a>
</li>