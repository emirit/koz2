<div id="movies-slideshow" class="carousel slide" data-pause="hover">
	<div class="carousel-inner">
		<!--div class="item active">
		<a href="/movie/haytarma/" title="Хайтарма">
			<img src="/img/haytarma.jpg" />
			<div class="carousel-caption">
			    <div class="pull-right slide-description">
		          <h4 class="arabic">Хайтарма</h4>
		          <p>2012, Украина, драма, исторический</p>
		        </div>
	        </div>
	    </a>
		</div-->
		<? $i = 0 ?>
		@foreach (Banner::get() as $banner)    			
		<div class="item <?if($i==0):?>active<?endif;?>">
		<a href="/movie/{{ $banner->movie()->first()->slug}}" title="{{ $banner->movie()->first()->name}}">
			<img src="/public/uploads/images/thumbs/680x340/{{ $banner->image }}" width="680" height="340" alt="{{ $banner->movie()->first()->name}}"/>
			<div class="carousel-caption">
			    <div class="pull-right slide-description">
		          <h4 class="arabic">{{ $banner->movie()->first()->name}}</h4>	
		          <p>{{ $banner->movie()->first()->year}}, {{ $banner->movie()->first()->countries()->first()->name }}, {{ $banner->movie()->first()->genres()->first()->name_single }}</p>	          
		        </div>		        
	        </div>
	    </a>
		</div>	
		<?$i++?>
		@endforeach 		    
	</div>
	<!-- Carousel nav -->
	<a class="carousel-control left" href="#movies-slideshow" data-slide="prev">&lsaquo;</a>
	<a class="carousel-control right" href="#movies-slideshow" data-slide="next">&rsaquo;</a>
</div>