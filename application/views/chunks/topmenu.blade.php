<div class="row">
	<div class="logo-block span3"><a class="logo" href="/">koz.tv</a></div>
	<div class="top-menu-block span9">
		<ul class="top-menu">
					@foreach ($categories as $category)
						<li><a href="/movies/category_{{ $category->slug }}/">{{ $category->name }}</a></li>
					@endforeach
		</ul>
	</div>
</div>