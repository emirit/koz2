<!DOCTYPE html>
<head>
	<title>{{ $meta_title }} | {{ __('common.site_name')}}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<style type="css">
	@import "http://webfonts.ru/import/intro.css";
	</style>
    	                                                                           
	{{ HTML::style('css/normalize.css') }}
	{{ HTML::style('css/bootstrap.css') }}
	
	{{ HTML::style('css/fonts.css') }}
	{{ HTML::style('css/styles.css') }} 
	
	{{ HTML::script('js/jquery-1.8.3.js') }}
	{{ HTML::script('js/bootstrap.js') }}
	
	<script type="text/javascript" src="/js/noUiSlider-3.2.1/jquery.nouislider.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/js/noUiSlider-3.2.1/nouislider.fox.css">                                                                                                
	
	{{ HTML::script('player/uppod-0.3.12.js') }}
	{{ HTML::script('player/swfobject.js') }}
	{{ HTML::script('js/scripts.js') }}
	
	
	                                                        
	<!--[if lt IE 9]>
	<link href="css/ie.css" rel="stylesheet">
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]--> 
</head>
	<body>
		<div class="container wrapper">	
