<form id="search_form" action="movies/"> 
		<div class="custom_select">		
	    <select class="add-to-path" id="category_slug" data-prefix="category">
	    	<option value="">{{ __('common.choose_category') }}</option>
	    	@foreach ($categories as $category)
			<option value="{{ $category->slug }}">{{ $category->name }}</option>
			@endforeach	    		    
	    </select>
	    </div> 
	    
	    <div class="custom_select">	   
	    <select class="add-to-path" id="genre_slug" data-prefix="genre">
	    	<option value="">{{ __('common.choose_genre') }}</option>
	    	@foreach ($genres as $genre)
			<option value="{{ $genre->slug }}">{{ $genre->name }}</option>
			@endforeach	 
	    </select> 
	    </div>  
	    
	    <div class="custom_select">	    
	    <select class="add-to-path" id="country_slug" data-prefix="country">
	    	<option value="">{{ __('common.choose_country') }}</option>
	    	@foreach ($countries as $country)
			<option value="{{ $country->slug }}">{{ $country->name }}</option>
			@endforeach	 
	    </select>
	    </div>
	    
	    <div class="custom_select">
	    
	    <select class="add-to-path" id="translation_slug" data-prefix="translation">
	    	<option value="">{{ __('common.choose_translation') }}</option>
	    	@foreach ($translations as $translation)
			<option value="{{ $translation->slug }}">{{ $translation->name }}</option>
			@endforeach	 
	    </select>                       
	    </div>
	    
	    <div class="date_input">
	    <label>{{ __('common.choose_year') }}: <span id="year-text" class="year">2000-2013</span></label>	    
	    
	    <!--select id="year" data-prefix="year"><option value=""></option></select-->	    
	    <div id="slider-year" class="noUiSlider connect year-selector"></div> 
	    <input type="hidden" id="year" class="add-to-path" data-prefix="year" value="2000-2013">
	    
	    </div>
	    
	    <div class="custom_input">	
	    <input id="name" type="text" class="add-to-path" data-prefix="name" placeholder="{{ __('common.enter_movie_name') }}" />
	    </div>
	    
	    <button type="button" id="button-reset" class="btn pull-left">{{ __('common.search_reset') }}</button>
	    <button type="submit" id="button-submit" class="btn btn-primary pull-right">{{ __('common.search_submit') }}</button>
	</form>
