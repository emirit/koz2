    	   </div>
    	   <div class="footer">
    		   <div class="container">
    			   <div class="row">
			            <div class="span3">
            				<h5>Смотрите</h5>
            				<ul>
            				@foreach ($categories as $category)
								<li><a href="/movies/category_{{ $category->slug }}/">{{ $category->name }}</a></li>
							@endforeach
							</ul>
			            </div>
			            <div class="span3">
            				<h5>KOZ.TV на устройствах</h5>
            				<ul>
            				<li><a href="/page/ios.html">iOS</a></li>             				
            				<li><a href="/page/android.html">Android</a></li>
            				<li><a href="/page/samsung.html">Samsung Smart TV</a></li>
            				<!--li><a href="">LG Smart TV</a></li-->
            				</ul>
			            </div>
			            <div class="span3">
            				<h5>О нас</h5>
            				<ul>
            				<li><a href="/page/about.html">О проекте</a></li>
            				<li><a href="/page/reklama.html">Размещение рекламы</a></li>
            				<li><a href="/page/pravoobladatelyam.html">Правообладателям</a></li>
            				<li><a href="/page/vakansii.html">Вакансии</a></li>
            				</ul>
			            </div>
			            <div class="span3">
            				<h5>Пользователям</h5>
            				<ul>
            				<li><a href="/page/user-agreement.html">Пользовательское соглашение</a></li>
            				
            				<li><a href="/page/feedback.html">Обратная связь</a></li>
            				
            				</ul>
			            </div>
		            </div>
	            </div>
           </div>
    	
	</body>
</html>