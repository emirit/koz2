<li class="portrait">
	<a href="/movie/{{ $movie->slug }}/" title="{{ $movie->name }}"> 		
		<div class="img" style="background-color: #ccc; background-image: url(/public/uploads/images/originals/{{ $movie->thumbnail }});"></div>
	</a>
		
		<div class="movie-info">
			<a class="movie-title" href="/movie/{{ $movie->slug }}/" title="{{ $movie->name }}">{{ $movie->name }}</a>
			<div class="movie-params">
    				{{ $movie->year }}, {{ $movie->countries()->first()->name }}, {{ $movie->genres()->first()->name_single }}
    		</div>    		
		</div>
<a href="/movie/{{ $movie->slug }}/" class="play-button"></a>	
</li>  

