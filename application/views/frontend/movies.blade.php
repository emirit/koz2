@include('chunks.head')  
@include('chunks.topmenu')

<div class="row main-content">
    
    <div class="span12 movies-result-list">
        <div class="main-search-top-inner"></div>
    	<div class="filter-buttons" data-spy="affix" data-offset-top="90">
    		<div class="btn-group">		    			
		        <a href="javascript:void(0)" class="btn btn-dotted dropdown-toggle" data-toggle="dropdown">
		        <span class="dots">
			        @if (isset($params['category'])) 
		        		{{ $categories[$params['category']]->name }} 
			        @else 
		        		{{ __('common.category') }} 
			        @endif 
		        </span> <span class="caret"></span></a>
		        <ul class="dropdown-menu">
		            @foreach ($categories as $category)
		            	<li>
		            		<a href="{{ Utils::create_url_for_filter('category', $category->slug) }}">{{ $category->name}}</a>
		            	</li>
		            @endforeach
		        </ul>
    		</div>
    		
    		<div class="btn-group">		    			
		        <a href="javascript:void(0)" class="btn btn-dotted dropdown-toggle" data-toggle="dropdown">
		        <span class="dots">
			        @if (isset($params['genre'])) 
		        		{{ Genre::where('slug', '=', $params['genre'])->first()->name }} 
			        @else 
		        		{{ __('common.genre') }} 
			        @endif 
		        </span> <span class="caret"></span></a>
		        <ul class="dropdown-menu">
		            @foreach ($genres as $genre)
		            	<li>
		            		<a href="{{ Utils::create_url_for_filter('genre', $genre->slug) }}">{{ $genre->name}}</a>
		            	</li>
		            @endforeach
		        </ul>
    		</div>
    		
    		<div class="btn-group">		    			
		        <a href="javascript:void(0)" class="btn btn-dotted dropdown-toggle" data-toggle="dropdown">
		        <span class="dots">
			        @if (isset($params['country'])) 
		        		{{ Country::where('slug', '=', $params['country'])->first()->name }} 
			        @else 
		        		{{ __('common.country') }} 
			        @endif 
		        </span> <span class="caret"></span></a>
		        <ul class="dropdown-menu">
		            @foreach ($countries as $country)
		            	<li>
		            		<a href="{{ Utils::create_url_for_filter('country', $country->slug) }}">{{ $country->name}}</a>
		            	</li>
		            @endforeach
		        </ul>
    		</div>
    		
    		<div class="btn-group">		    			
		        <a href="javascript:void(0)" class="btn btn-dotted dropdown-toggle" data-toggle="dropdown">
		        <span class="dots">
			        @if (isset($params['translation'])) 
		        		{{ Translation::where('slug', '=', $params['translation'])->first()->name }} 
			        @else 
		        		{{ __('common.translation') }} 
			        @endif 
		        </span> <span class="caret"></span></a>
		        <ul class="dropdown-menu">
		            @foreach ($translations as $translation)
		            	<li>
		            		<a href="{{ Utils::create_url_for_filter('translation', $translation->slug) }}">{{ $translation->name}}</a>
		            	</li>
		            @endforeach
		        </ul>
    		</div>
    	
    	</div>
    	
    	<ul class="movies-list" style="margin-top: 60px;">
    	@foreach ($movies as $movie)
    		@include('chunks.movie_item_portrait')
    	@endforeach    		
    	</ul>
    	<div class="clearfix"></div>         	
    
    </div>  
    
 
    
    	
</div>       

 
@include('chunks.foot') 
