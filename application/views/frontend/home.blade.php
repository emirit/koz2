@include('chunks.head')
@include('chunks.topmenu')

<div class="second-row clearfix">
	<div class="main-search">
			<div class="main-search-top"></div>		
			@include('chunks.main_search_form')	
			<div class="main-search-triangle"></div>	 
			<div class="main-search-bottom"></div>                               		
	</div>
	<div class="top-slideshow">
		@include('chunks.slideshow')
	</div>
</div>

  
<div class="main-content">

        <div class="main-content-block">
    		<h3 class="title-big arabic">Рекомендуем посмотреть</h2>
    		<ul class="movies-list">
    		@foreach (Movie::where_featured(true)->take(6)->get() as $movie)
    			@include('chunks.movie_item_portrait')
    		@endforeach    		
    		</ul>
    	</div> 
    	
    	<div class="main-content-block">
    		<h3 class="title-big arabic">Новые поступления</h2>
    		<ul class="movies-list">
    		@foreach (Movie::order_by('created_at', 'desc')->take(18)->get() as $movie)
    			@include('chunks.movie_item_portrait')
    		@endforeach    		
    		</ul>
    	</div>          	



   	
</div>       

@include('chunks.foot')

 

              