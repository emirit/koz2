@include('chunks.head')

@include('chunks.topmenu')

<div class="row main-content">

<div class="main-search-top-movie"></div>
	<div class="span12 video-container" >
		<div id="koz-player"></div>           
	</div>  
<script type="text/javascript">
	var ua = navigator.userAgent.toLowerCase();
	var flashInstalled = false;
	if (typeof(navigator.plugins)!="undefined" && typeof(navigator.plugins["Shockwave Flash"])=="object") { 
		 flashInstalled = true; 
	} else if (typeof  window.ActiveXObject !=  "undefined") { 
		try { 
			if (new ActiveXObject("ShockwaveFlash.ShockwaveFlash")) { 
				flashInstalled = true; 
			} 
		} catch(e) {}; 
	};
	  
	if(ua.indexOf("iphone") != -1 || ua.indexOf("ipad") != -1 || (ua.indexOf("android") != -1 && !flashInstalled)){

		// HTML5
		this.videoplayer = new Uppod({m:"video",uid:"koz-player",file:"{{ $movie->video_remote }}", comment:"{{ $movie->name }}", poster: '/public/uploads/images/originals/{{ $movie->thumbnail }}' });
		
	}else{
	  if(!flashInstalled){
		 // NO FLASH
		 document.getElementById("videoplayer").innerHTML="<a href=http://www.adobe.com/go/getflashplayer>Требуется установить Flash-плеер</a>";
		 
	  }else{
	  
		 // FLASH
		 var flashvars = {
			"comment":"{{ $movie->name }}",
			"st":"42AEEAG89v5f=JaYoAjYT42Qi63GAnasQX6BGJRCQ3nHB0IRkGcLkUEnoW5ntu0c9BMrB",
			"file":"{{ $movie->video_remote }}"		
		};
		var params = {wmode:"transparent", allowFullScreen:"true", allowScriptAccess:"always",id:"koz-player"}; 
		new swfobject.embedSWF("http://koz.tv/uppod/uppod.swf", "koz-player", "960", "500", "9.0.115.0", false, flashvars, params);
	  }
	}



</script>      
	
	
	<div class="span9 right-column">   
		<div class="movie-top">
			<h1 class="movie-header arabic">{{ $movie->name }}</h1>
			@if ( isset($movie->age)) 
				<div class="movie-age-limit">{{ $movie->age }}+</div>				 
			@endif
		</div>
		<p class="lead name-original">{{ $movie->name_original }}</p>
		<div class="row">
			<div class="span2">
				<img class="movie-image" src="/public/uploads/images/thumbs/portrait/{{ $movie->thumbnail }}" alt="{{ $movie->name }}" />			
			</div>
			
			<div class="span7">
				<ul class="movie-attributes">
					<li><span class="key">{{ __('common.genre') }}:</span>
						<span class="value">
							@foreach ($movie->genres()->get() as $genre)
    						<a class="label genre" href="/movies/genre_{{ $genre->slug }}">{{ $genre->name_single }}</a>
    						@endforeach
						</span>
					</li>
					<li><span class="key">{{ __('common.country') }}:</span>
						<span class="value">
							@foreach ($movie->countries()->get() as $country)
    						<a class="label label-info country" href="/movies/country_{{ $country->slug }}">{{ $country->name }}</a>
    						@endforeach
						</span>
					</li>
					<li><span class="key">{{ __('common.translation') }}:</span>
						<span class="value">
							@foreach ($movie->translations()->get() as $translation)
    						<a class="label label-success translation" href="/movies/translation_{{ $translation->slug }}">{{ $translation->name }}</a>
    						@endforeach
						</span>
					</li>
					
					<li><span class="key">{{ __('common.year') }}:</span>
						<span class="value">
							<span class="label label-warning year">{{ $movie->year }}</span>
						</span>
					</li> 
					
					
				</ul>
				
				<div class="movie-buttons">
				
				<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
                <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
				
				<!--
					<button class="btn btn-primary"><i class="icon-eye-open icon-white"></i> Смотреть</button>
					<button class="btn"><i class="icon-heart"></i> В избранное</button>
				-->				
				</div>
				
				
			</div>
		</div>
		<div class="row">
			<div class="span9 movie-description">
			{{ $movie->description }}
			</div>
		</div>
	</div>
	<!--div class="span3 left-column">
    	<h3 class="arabic">{{ __('common.related_items') }}</h3>
    </div-->
    	
  
</div>

 
@include('chunks.foot') 