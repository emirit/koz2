<?php




class Utils
{ 	 
	
	public static function make_slug_keys($objects_array)
	{
		$result = array();
		foreach($objects_array as $object)
		{
			if(!$object->slug)
			{
				return false;
			}
			$result[$object->slug] = $object;
		}

		return $result;
	}
	
	public static function create_url_for_filter($filter_key, $filter_value)
  	{
  	 	 $url =  str_replace(URI::segment(1), '', URI::current());
  	 	 
  	 	 $segments = explode('/', $url);
  	 	 
  	 	 $new_segments = array();
  	 	 $key_found = false;
  	 	 
  	 	 foreach($segments as $segment)
  	 	 {
  	 	 	 if(!$segment)
  	 	 	 {
				 continue;
  	 	 	 }
			 $tmp = explode('_', $segment);
			 $key = $tmp[0];
			 $value = $tmp[1];
			 
			 if($key == $filter_key)
			 {
				 $value = $filter_value;
				 $key_found = true;
			 }
			 
			 $new_segments[] = $key . '_' . $value;
  	 	 }
  	 	 
  	 	 $url = '/' . URI::segment(1) . '/' .implode('/', $new_segments) . '/' . ($key_found ? '' : ($filter_key . '_' . $filter_value)) ;
  	 	 
  	 	 
  	 	 return $url;
		 
  	}
  	
  	public static function translit($string)
  	{
  		 $tr = array(
	        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
	        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
	        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
	        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
	        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
	        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
	        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya", 
	        " "=> "-", "."=> "-", "/"=> "-" , "'"=> ""
    		);
    		
    		return preg_replace('/[^A-Za-z0-9_\-]/', '', strtr($string,$tr)); 
  	}
}