<?php

/**
 * Genres model config
 */

return array(

	'title' => __('labels.season_capital')->get(),

	'single' => __('labels.season')->get(),

	'model' => 'Season',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'name' => array(
			'title' => __('labels.name')->get(),			
		), 
		'movie' => array(
			'title' => __('labels.season_capital')->get(),
			'relationship' => 'movie',
			'select' => '(:table).name'			
		),
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'name' => array(
			'title' => __('labels.name')->get(),
		),   
		
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'name' => array(
			'title' => __('labels.name')->get(),
			'type' => 'text',
		),
		'movie' => array(
			'title' => __('labels.movie_capital')->get(),
			'type' => 'relationship',
			'name_field' => 'name'
		),
		
	),

);
