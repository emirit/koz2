<?php

/**
 * Genres model config
 */

return array(

	'title' => __('labels.genres_capital')->get(),

	'single' => __('labels.genre')->get(),

	'model' => 'Genre',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'name' => array(
			'title' => __('labels.name')->get(),			
		),
		'slug' => array(
			'title' => __('labels.slug')->get(),			
		), 
		'on_main' => array(
			'title' => 'On main',			
		),
		'num_movies' => array(
			'title' => '# movies',
			'relationship' => 'movies',
			'select' => 'COUNT((:table).id)',
		),
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'name' => array(
			'title' => __('labels.name')->get(),
		),
		'slug' => array(
			'title' => __('labels.slug')->get(),
		), 
		'on_main' => array(
			'title' => 'On main',			
		),
		
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'name' => array(
			'title' => __('labels.name')->get(),
			'type' => 'text',
		),
		'name_single' => array(
			'title' => __('labels.name_single')->get(),
			'type' => 'text',
		),
		'slug' => array(
			'title' => __('labels.slug')->get(),
			'type' => 'text',
		),  
		'on_main' => array(
			'title' => 'On main',			
			'type' => 'bool',			
		),
		'description' => array(
			'title' => 'Description',
			'type' => 'textarea',
		),
		
	),

);
