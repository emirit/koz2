<?php

/**
 * Categories model config
 */

return array(

	'title' => __('labels.categories_capital')->get(),

	'single' => __('labels.category')->get(),

	'model' => 'Category',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'name' => array(
			'title' => __('labels.name')->get(),			
		),
		'slug' => array(
			'title' => __('labels.slug')->get(),			
		), 
		
		'num_movies' => array(
			'title' => '# movies',
			'relationship' => 'movies',
			'select' => 'COUNT((:table).id)',
		),
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'name' => array(
			'title' => __('labels.name')->get(),
		),
		'slug' => array(
			'title' => __('labels.slug')->get(),
		), 
		
		
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'name' => array(
			'title' => __('labels.name')->get(),
			'type' => 'text',
		),
		'slug' => array(
			'title' => __('labels.slug')->get(),
			'type' => 'text',
		),  
		
		'description' => array(
			'title' => __('labels.description')->get(),
			'type' => 'textarea',
		),
		
	),

);
