<?php

/**
 * Movies model config
 */

return array(

	'title' => __('labels.movies_capital')->get(),

	'single' => __('labels.movie')->get(),

	'model' => 'Movie',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'name' => array(
			'title' => __('labels.name')->get(),			
		),
		'slug' => array(
			'title' => __('labels.slug')->get(),			
		), 
		
		'category' => array(
			'title' => __('labels.category_capital')->get(),
			'relationship' => 'category',
			'select' => '(:table).name'			
		),
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'name' => array(
			'title' => __('labels.name')->get(),
		),
		'slug' => array(
			'title' => __('labels.slug')->get(),
		), 
		
		
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'name' => array(
			'title' => __('labels.name')->get(),
			'type' => 'text',
		),
		'name_original' => array(
			'title' => __('labels.name_original')->get(),
			'type' => 'text',
		),
		'slug' => array(
			'title' => __('labels.slug')->get(),
			'type' => 'text',
		),
		
		'featured' => array(
		    'type' => 'bool',
		    'title' => __('labels.on_main')->get(),
		) ,
		
		'thumbnail' => array(
			'title' => __('labels.thumbnail')->get(),
			'type' => 'image',
    		'location' => path('public') . 'uploads/images/originals/',
    		'naming' => 'random',
    		'length' => 20,
    		'size_limit' => 2,
    		'sizes' => array(
        		array(124, 174, 'crop', path('public') . 'uploads/images/thumbs/portrait/', 100),
        		array(220, 100, 'crop', path('public') . 'uploads/images/thumbs/landscape/', 100),        		
    			)
    	),
    	
    	'video' => array(
		    'title' => __('labels.file')->get(),
		    'type' => 'file',
		    'location' => path('storage') . 'videos/',
		    'naming' => 'random',
		    'length' => 20,
		    'size_limit' => 20000,
		),
		
		'video_embed' => array(
			'title' => __('labels.video_embed')->get(),
			'type' => 'textarea',
		),
		
		'video_remote' => array(
			'title' => __('labels.video_remote')->get(),
			'type' => 'textarea',
		),

		
		'description' => array(
			'title' => __('labels.description')->get(),
			'type' => 'textarea',
		),
		
		'year' => array(
			'title' => __('labels.year ')->get(),
			'type' => 'text',
		),
		
		'age' => array(
			'title' => __('labels.age')->get(),
			'type' => 'text',
		),
		
		'category' => array(
			'title' => __('labels.category_capital')->get(),
			'type' => 'relationship',
			'name_field' => 'name'
		),
		
		'genres' => array(
			'title' => __('labels.genres_capital')->get(),
			'type' => 'relationship',
			'name_field' => 'name'
		), 
		
		'countries' => array(
			'title' => __('labels.countries_capital')->get(),
			'type' => 'relationship',
			'name_field' => 'name'
		),
		/*
		'languages' => array(
			'title' => 'Language',
			'type' => 'relationship',
			'name_field' => 'name'
		), 	 
		
		*/
		'translations' => array(
			'title' => __('labels.translations_capital')->get(),
			'type' => 'relationship',
			'name_field' => 'name'
		),
		
		'tags' => array(
			'title' => __('labels.tags_capital')->get(),
			'type' => 'relationship',
			'name_field' => 'name',
			'autocomplete' => true,
    		'num_options' => 5, //default is 10
    		'search_fields' => array("name"),
		),
		
	),

);

