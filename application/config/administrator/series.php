<?php

/**
 * Genres model config
 */

return array(

	'title' => __('labels.series_capital')->get(),

	'single' => __('labels.serie')->get(),

	'model' => 'Serie',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'name' => array(
			'title' => __('labels.name')->get(),			
		), 
		'season' => array(
			'title' => __('labels.season_capital')->get(),
			'relationship' => 'season',
			'select' => '(:table).name'			
		),
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'name' => array(
			'title' => __('labels.name')->get(),
		),   
		
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'name' => array(
			'title' => __('labels.name')->get(),
			'type' => 'text',
		),
		'season' => array(
			'title' => __('labels.season_capital')->get(),
			'type' => 'relationship',
			'name_field' => 'name'
		),
		
	),

);
