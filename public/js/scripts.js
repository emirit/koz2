function fillSelect(prefix)
{
	$('#' + prefix + '_slug').find('option[value!=""]').detach();
			$.ajax
			({
				url: "all/" + prefix,			
				dataType: 'json',
				success: function(data)
				{
					$.each(data, function(key, item)
					{ 				
						$('#' + prefix + '_slug').append('<option value="' + item.slug + '">' + item.name + '</option>');		
					})
				}
			});	
}

$(document).ready(function()
{
	$('#search_form select, #search_form input').on('change', function()
	{   
		var curSelect = $(this);
		var curValue = $(this).val();
		
		if($(this).val())		
		{
			$.ajax
			({
				url: "build_filter/" + $(this).data('prefix') + '_' + $(this).val(),			
				dataType: 'json',
				success: function(data)
				{   										   
					$('#search_form select').each(function()
					{	
						$(this).data('value', $(this).val());
						if($(this).data('prefix') != curSelect.data('prefix'))
						{
							$(this).find('option[value!=""]').detach();							
						}					
						
					})
										
					$.each(data, function(key, row) 
					{  
							$.each(row, function(slug, name)
							{
								if(key != curSelect.data('prefix') + '_slug')
								{
									$('#' + key).append('<option value="' + slug + '">' + name + '</option>');
								}
							})	
																	
					})	
					
					$('#search_form select').each(function()
					{							
						$(this).find('option[value="' + $(this).data('value') + '"]').attr('selected', 'selected');						
					})
				}
			});	
		}
			
		
	})
	
	$('#button-reset').on('click', function()
	{
		$('#search_form input').val('');
		
		fillSelect('category');
		fillSelect('genre');
		fillSelect('translation');
		fillSelect('country');
				
	})  
	  
	$('#search_form').on('submit', function()
	{
		var path = '';
		
		$('#search_form .add-to-path').each(function()
		{
			if($(this).val() != '')
			{
				path += $(this).data('prefix') + '_' + $(this).val() + '/';
			}
		})  		
		
		$('#search_form').attr('action', 'movies/' + path );		
	})
	
	
	$(".noUiSlider").noUiSlider({ 
	    range: [1950, 2013]
	   ,start: [2000, 2013]
	   ,step: 1
	   ,slide: function(){
	      var values = $(this).val();
	      $('#year').val(values[0] + '-' + values[1]);
	      $('#year-text').text(values[0] + '-' + values[1]);
	   }

	}); 

	
})