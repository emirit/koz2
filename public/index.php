<?php  
/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @version  3.2.14
 * @author   Taylor Otwell <taylorotwell@gmail.com>
 * @link     http://laravel.com
 */

require_once '../application/libraries/mobileDetect/Mobile_Detect.php';

$detect = new Mobile_Detect;
if($detect->isMobile() && !strstr($_SERVER['REQUEST_URI'], 'build_filter') && !strstr($_SERVER['REQUEST_URI'], 'movies_json') && !strstr($_SERVER['REQUEST_URI'], 'movie_json'))
{
	header('Location: http://m.koz.tv/');
	die();
}
	
 
// --------------------------------------------------------------
// Tick... Tock... Tick... Tock...
// --------------------------------------------------------------
define('LARAVEL_START', microtime(true));

// --------------------------------------------------------------
// Indicate that the request is from the web.
// --------------------------------------------------------------
$web = true;

// --------------------------------------------------------------
// Set the core Laravel path constants.
// --------------------------------------------------------------
require '../paths.php';

// --------------------------------------------------------------
// Unset the temporary web variable.
// --------------------------------------------------------------
unset($web);

// --------------------------------------------------------------
// Launch Laravel.
// --------------------------------------------------------------
require path('sys').'laravel.php';