<?php

class LaraAdmin
{

	public static function make()
	{
		// Config::set('laraAdmin.models', array() );
		Config::set(
			'laraAdmin.models',
			array(
				'Movie','Genre','Category','Country', 'Language', 'Tag'
			)
		);
		Config::set('laraAdmin.title', "Admin panel");
	}

}