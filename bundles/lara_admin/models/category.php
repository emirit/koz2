<?php namespace Admin; 
 class Category extends Appmodel{ 
 	public static $table ='categories';  
 	public $index= array('id', 'name' ,'slug');  
 	public $new=array();  
 	public $edit= array(
 		'name' => array('required' => true),
 		'slug' => array('required' => true), 		
 		'description' => array("type"=>"textarea"), 		
 	);    
 	public $show= array();  
 	public $rules= array();  
 }