<?php namespace Admin; 
 class Country extends Appmodel{ 
 	public static $table ='countries';  
 	public $index= array('id', 'name' ,'slug');  
 	public $new=array();  
 	public $edit= array(
 		'name' => array('required' => true),
 		'slug' => array('required' => true),		
 		'description' => array("type"=>"textarea"), 		
 	);    
 	public $show= array();  
 	public $rules= array();  
 }