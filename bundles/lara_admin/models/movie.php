<?php namespace Admin; 
 class Movie extends Appmodel{ 
 	public static $table ='movies';  
 	public $index= array('id', 'name', 'slug', 'featured');  
 	public $new=array();  
 	public $edit= array();  
 	public $show= array();  
 	public $rules= array();  
 }