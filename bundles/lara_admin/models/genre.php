<?php namespace Admin; 
 class Genre extends Appmodel{ 
 	public static $table = 'genres';  
 	public $index = array('id', 'name',
 		'slug',
 		'on_main');  
 	public $new =array();  
 	public $edit = array(
 		'name' => array('required' => true),
 		'slug' => array('required' => true),
 		'on_main' => array('type' => 'checkbox'),
 		'description' => array("type"=>"textarea"), 		
 	);  
 	public $show = array();  
 	public $rules = array();  
 	
 }