<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Administrator Language Lines
	|--------------------------------------------------------------------------
	|
	| ru - Русский
	|
	*/

	'dashboard' => 'Панель управления',
	'edit' => 'Изменить',
	'filters' => 'Фильтры',
	'loading' => 'Загрузка...',
	'createnew' => 'Создать',
	'new' => 'Новый',
	'viewitem' => 'Просмотр :single',
	'id' => 'ID',
	'uploadimage' => 'Загрузить картинку',
	'imageuploading' => 'Загрузка картинки',
	'uploadfile' => 'Загрузить файл',
	'fileuploading' => 'Загрузка файла',
	'none' => 'Нет',
	'all' => 'Все',
	'itemsperpage' => 'элементов на странице',
	'noresults' => 'Нет резйльтатов',
	'backtosite' => 'Назад на сайт',

	'previous' => 'предыдущий',
	'next' => 'следующий',

	'close' => 'Закрыть',
	'delete' => 'удалить',
	'save' => 'Сохранить',
	'create' => 'Создать',
	'cancel' => 'Отменить',

	'active' => 'Секундочку...',
	'success' => 'Успех!',
	'error' => 'во время выполнения возникла ошибка',

	'valid_columns' => "Укажите верный 'columns' массив в конфиге для каждой модели",
	'valid_title' => "You must provide a valid title and single name in each model's config",
	'valid_model' => "You must provide a 'model' option in each model's config",
	'valid_edit' => "You must provide a valid 'edit_fields' array in each model's config",
	'valid_menu' => "You must provide a valid 'menu' option in the administrator.php config",
	'valid_config_path' => "You must provide a valid 'model_config_path' in the administrator.php config. The directory must also exist and be readable.",
	'not_eloquent' => " is not an Eloquent model",
	'storage_path_permissions' => "You must make your storage path writable in order to make a settings page",
	'valid_home_page' => "You must provide a valid menu item in the home_page option of your config",
);
